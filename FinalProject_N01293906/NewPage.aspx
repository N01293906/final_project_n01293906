﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="NewPage.aspx.cs" Inherits="FinalProject_N01293906.NewPage" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
 <asp:SqlDataSource runat="server" id="create_page"
        ConnectionString="<%$ ConnectionStrings:blog_sql_con %>">
    </asp:SqlDataSource>

    <h3>New Page</h3>
    <form class="form-horizontal">
    <div class="form-group">
        <label class="control-label col-sm-2">Page Title:</label>
        <asp:textbox id="page_title" class="form-control" runat="server"> </asp:textbox>
        <asp:RequiredFieldValidator ID="validator_page_title" ForeColor="Red" runat="server" ErrorMessage="Must Enter a Title" ControlToValidate="page_title"></asp:RequiredFieldValidator> 
    </div>

    <div class="form-group">
        <label class="control-label col-sm-2">Author Name:</label>
        <asp:textbox id="page_author" class="form-control" runat="server"></asp:textbox>
        <asp:RequiredFieldValidator ID="validator_page_author" ForeColor="Red" runat="server" ErrorMessage="Must Enter a Author" ControlToValidate="page_author"></asp:RequiredFieldValidator>
    </div>

    <div class="form-group">
        <label class="control-label col-sm-2">Content:</label>
        <asp:textbox id="page_content" class="form-control" TextMode="multiline" runat="server">
        </asp:textbox>
    </div>
<div class="form-group"> 
    <div class="col-sm-offset-2 col-sm-10">
    <asp:Button Text="Create" class="btn btn-success"  runat="server" OnClick="CreatePage"/>
        </div>
    </div>
    </form>
    <!--<div id="Query1" runat="server"></div>-->

</asp:Content>
