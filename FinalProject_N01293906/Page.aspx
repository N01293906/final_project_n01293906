﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Page.aspx.cs" Inherits="FinalProject_N01293906.Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
   <asp:SqlDataSource runat="server"
        id="page_select"
        ConnectionString="<%$ ConnectionStrings:blog_sql_con %>">
    </asp:SqlDataSource>
    <div class="page-header"><h1 runat="server" id="page_title"></h1></div>
    <br />
    <h3 id="page_date" runat="server"></h3>
    <%-- sql data source for the delete --%>
    <asp:SqlDataSource 
        runat="server"
        id="del_page"
        ConnectionString="<%$ ConnectionStrings:blog_sql_con %>">
    </asp:SqlDataSource>
    <div  id="inputrow">
    <asp:Button runat="server" id="del_page_btn"
       OnClientClick="if(!confirm('Are you sure?')) return false;"
        Text="Delete" OnClick="FetchID"  class="btn btn-danger"/>
    <a class="btn btn-primary" href="EditPage.aspx?pageid=<%Response.Write(this.pageid);%>">Edit</a> 
    </div>
    <%-- 
        Connection string for built in SQL system
    --%>
     

    <%-- container for representing class info --%><br />
      <div class="panel panel-primary"><p runat="server" id="page_content" class="panel-body"></p></div>
       
</asp:Content>
