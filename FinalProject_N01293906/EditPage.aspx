﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="EditPage.aspx.cs" Inherits="FinalProject_N01293906.EditPage" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
        <%-- One SQL command for viewing the existing info --%>
    <asp:SqlDataSource runat="server" id="page_select"
        ConnectionString="<%$ ConnectionStrings:blog_sql_con %>">
    </asp:SqlDataSource>

    <%-- One SQL command for editing the student --%>
    <asp:SqlDataSource runat="server" id="edit_page"
        ConnectionString="<%$ ConnectionStrings:blog_sql_con %>">
    </asp:SqlDataSource>

    <h3 runat="server" id="page_name">Update </h3>
    <form id="form1">
    <div class="form-group">
        <label>Page Title:</label>
        <asp:textbox id="page_title" class="form-control"  runat="server">
        </asp:textbox>
        <asp:RequiredFieldValidator ID="validator_page_title" ForeColor="Red" runat="server" ErrorMessage="Must Enter a Title" ControlToValidate="page_title"></asp:RequiredFieldValidator> 
    
        
    </div>

    <div class="form-group">
        <label>Author:</label>
        <asp:textbox id="page_author" class="form-control"  runat="server">
        </asp:textbox>
        <asp:RequiredFieldValidator ID="validator_page_author" ForeColor="Red" runat="server" ErrorMessage="Must Enter a Author" ControlToValidate="page_author"></asp:RequiredFieldValidator>
    
    </div>

    <div class="form-group">
        <label>Page Content:</label>
        <asp:textbox id="page_content" class="form-control"  TextMode="multiline" Columns="50" Rows="5" runat="server">
        </asp:textbox>
    </div>
    <asp:Button Text="Edit" runat="server" class="btn btn-primary" OnClick="Edit_Page"/>
    </form>

</asp:Content>
