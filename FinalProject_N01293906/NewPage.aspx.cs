﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FinalProject_N01293906
{
    public partial class NewPage : System.Web.UI.Page
    {
        private string addquery = "INSERT INTO BLOGPAGES " +
            "(PAGETITLE, PAGEAUTHOR, PAGECONTENT, PAGEDATE)" +
            " VALUES ";
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void CreatePage(object sender, EventArgs e)
        {
            string p_title = page_title.Text;
            string p_author = page_author.Text;
            string p_content = page_content.Text;
            //from a date time > putting it into a string and
            //converting it back into a datetime in our insert statement.
            DateTime now = DateTime.Today;
            string today=now.Date.ToString("dd/MM/yyyy");


            addquery += " ('"+p_title+"', '"+p_author+"', '"+p_content+"',convert(DATETIME,'" + today+"',103))";

            create_page.InsertCommand = addquery;
            Query1.InnerHtml = addquery;
            //create_page.Insert();
            Response.Redirect("Pages.aspx");


        }
    }
}