﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Pages.aspx.cs" Inherits="FinalProject_N01293906.Pages" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
 <h2><%: Title %></h2>
    <!-- interface for adding new students -->
    <div id="control_div">
        <div id="inputrow"><a href="NewPage.aspx" class="btn btn-primary">New Page</a>
        </div>
    <div id="search_div">
    <form class="form-inline">
        <div class="form-group">
    <!-- Interface for searching BLOGPAGES records-->
    <asp:TextBox runat="server" form-control ID="page_key"></asp:TextBox>
    <asp:Button runat="server" class="btn btn-success" Text="Search" OnClick="Search_Pages"/>
    </div>
    </form>
    </div>
    <!--<div id="qdisplay" runat="server"></div>-->
    <%-- 
        Connection string for built in SQL system
    --%>
    

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="fullscreen" runat="server">
<asp:SqlDataSource runat="server"
        id="pages_select"
        ConnectionString="<%$ ConnectionStrings:blog_sql_con %>">
    </asp:SqlDataSource>
    <asp:DataGrid id="pages_list"
        runat="server" class="table" >
    </asp:DataGrid>
</asp:Content>
